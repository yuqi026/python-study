# 命名函数
def polynomial(x):
    return x ** 2 + 5 * x + 4


print(polynomial(-4))

# lambda
print((lambda x: x ** 2 + 5 * x + 4)(1))
